# Introduction

This project demonstrates how to build Continuous Integration (CI)/Continuous Deployment (CD) infrastructure to automate RFSoC 4x2 Petalinux BSP building and in-hardware testing. The concepts demonstrated here could be rather easily expanded to other SoC based devices like AMD-Xilinx Zynq MPSoC and Versal.

The project consists mainly documentation, gitlab CI pipeline definitions, and Petalinux build scripts.

For the purpose of the demonstration, the Vivado build process is skipped and instead we use precompiled XSA file. We focus on 2020.2 version of the tools. The XSA file utilized for the demonstrations originaly comes from the RFSoC 4x2 BSP provided on [this github repository](https://github.com/RealDigitalOrg/RFSoC4x2-BSP/releases/tag/v2020.2).

The slides showcasing this work were presented in-person at CERN [SoC Interest Group Meeting, on 23 November 2022](https://indico.cern.ch/event/1208190) , and can be downloaded using [this direct link](https://indico.cern.ch/event/1208190/contributions/5088858/attachments/2553351/4399287/MHusejko-CERN-SoC-IGM-2022-NOV-Presented.pdf).

# Support of multiple BSP versions

This single gitlab project supports building multiple RFSoC4x2 BSP versions based on the same input XSA file.

Three BSP version are supported, each configured for network boot:
- `basic` - basic BSP functionality.
- `docker` - basic with added docker engine.
- `k8s` - basic with docker and kubernetes.


# Disclaimer

No SD Cards were harmed while developing this open source contribution.

# Acknowledgement

I would like to thank Xilinx University Program (XUP) Team for supporting this research.


# RFSoC 4x2 CI/CD infrastructure

The RFSoC 4x2 BSP Continuous Integration (CI) and Continous Deployment (CD) infrastructure is based on these building blocks:
- RFSoC-4x2 board.
- Ubuntu Linux Workstation.
- Power outlet controlled over Ethernet.
- gitlab (server) repository.
- network attached artifacts' storage.


## RFSoC-4x2 board

The board is configured for JTAG boot. Power adapter connected to power outlet. Power outlet can be switched on/off over Ethernet either by developer or by a script from within CI pipeline/job.

## Ubuntu Linux workstation

The workstation is configured to be used for local development and at the same time be able to run gitlab CI jobs. It is configured with:
- 2020.2 Vivado and Petalinux installed and configured.
- standard embedded systems development config i.e.:
  - DHCP server.
  - TFTP server
  - NFS export.
  - JTAG hardware server.
- gitlab-runner installed and enabled to accept CI jobs.
- access to network attached storage to store artifacts produced by CI/CD flow.

![alt text](doc/fig/rfsoc4x2-gitlab-ci-lab.png "Title Text")

# gitlab CI/CD setup

## Build stages and artifacts

The CI/CD flow consists three main stages:
- `Stage 1` - Execute Vivado build to generate .xsa/.bit/.ltx files.
- `Stage 2` - Build Petalinux boot images based on .xsa file.
- `Stage 3` - Boot the RFSoC4x2 board using JTAG+TFTP+NFS boot. Run the tests.
- If Stage 3 is successful then allow to manually launch `Stage 4` which will build SD Card image. This stage is still work in progress.

Stages presented on the diagram below.

![alt text](doc/fig/rfsoc4x2-gitlab-ci-stor.png "Title Text")

## gitlab CI variables

gitlab CI variables utlized by the build scripts are as folows:

![alt text](doc/fig/gitlab-ci-variables.png "Title Text")

Variables can be created by navigiating to gitlab project "Settings", then "CI/CD" section, and "Variables" sub-section.

Their usage is as follows:

- CI_MK_PASSWD_ROOT - password for root user. Will be injected into boot images.
- CI_MK_PASSWD_USR - password for soc-usr user. Will be injected into boot images.
- CI_MK_RFSOC_IP - IP address of the RFSoC 4x2 board.
- NETIO_IPADDR - IP address of the Ethernet controlled power outlet.
- NETIO_OUTPUT - power outlet output number, where the kit is pluged in.
- NETIO_PASS - power outlet access password.

## gitlab CI pipeline

The main gitlab CI/CD pipeline is presented below.

![alt text](doc/fig/rfsoc4x2-gitlab-ci-pipeline.png "Title Text")

We are using precompiled XSA file hence Vivado CI stage is just a dummy stage.

`Petalinux` stage builds all images required to network boot the RFSoC 4x2 board.

The `Pwron` stage performs power on of the board.

The `Prog` stage populates tftp and NFS export folders with the required content and then loads initial images over JTAG, which triggers network boot of the board.

After the board is booted a set of `Tests` are performed on that particular build.

After the tests are finished the board could by inspected ineractively or power off could be triggered by executing manual stage `Pwroff`.


## How to launch a BSP build ?

Navigate to gitlab CI pipelines and click on *Run PIpeline* button.

![alt text](doc/fig/gitlab-ci-run-pipeline.png "Title Text")

A new GUI interface will open allowing to submit a pipeline with option to add variable(s) with corresponding value(s).

![alt text](doc/fig/gitlab-ci-run-pipeline-2.png "Title Text")

Fill `Input Variable Key` field with `ZYNQ_BSP`, and `Input variable value` with type of BSP to build i.e.: `basic`, `docker`, or `k8s`. Then submit the pipeline with `Run pipeline` button.
