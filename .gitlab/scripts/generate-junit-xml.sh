
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" > report.xml
echo "<testsuites id=\"Tests for RFSoC 4x2 board\" name=\"Test summary\" tests=\"1\" failures=\"0\">" >> report.xml
echo "  <testsuite id=\"Format-master\" name=\"${1}\" tests=\"1\" failures=\"0\">" >> report.xml
echo "    <testcase name=\"[${2}]\" status=\"passed\" file=\"status.log\" time=\"120\">" >> report.xml

# Fill error message
if grep -q "FAILED" "status.log";then
    echo -e "      <error message=\"test failed\" type=\"error\">" >> report.xml
    echo "Test ${2} failed. Please check pipeline ${CI_PIPELINE_ID}, job ${CI_JOB_ID}" >> report.xml
#    grep "# ERROR " ${TEST_NAME}.log >> report.xml
    echo    "      </error>" >> report.xml
fi

echo "    </testcase>" >> report.xml
echo "  </testsuite>" >> report.xml
echo "</testsuites>" >> report.xml


if grep -q "FAILED" "status.log";then
    echo "RFSoC 4x2 test failed - tst name: ${2}"
    exit 1
else
    echo "RFSoC 4x2 test passed - tst name: ${2}"
fi
