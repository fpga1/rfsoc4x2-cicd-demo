#
# This file is the mhcicd recipe.
#

SUMMARY = "Simple mhcicd application"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://id_rsa.pub \
	"

S = "${WORKDIR}"

USER="soc-usr"

do_install() {
             install -d ${D}/home/${USER}/.ssh/
             install -m 0755 ${S}/id_rsa.pub ${D}/home/${USER}/.ssh/
             install -m 0755 ${S}/id_rsa.pub ${D}/home/${USER}/.ssh/authorized_keys
}

FILES_${PN} += "/home/${USER}/.ssh/*"

