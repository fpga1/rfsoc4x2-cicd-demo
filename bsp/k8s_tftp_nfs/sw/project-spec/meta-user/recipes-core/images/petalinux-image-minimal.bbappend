# petalinux-user-image.bbappend content
inherit extrausers

EXTRA_USERS_PARAMS = "\
    usermod -P MK_PASSWD_ROOT root; \
    useradd -P MK_PASSWD_USR soc-usr; \
    usermod -aG docker soc-usr; \
    "
